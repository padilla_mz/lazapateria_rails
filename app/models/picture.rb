class Picture < ActiveRecord::Base
  belongs_to :post

  # default_scope {order('image_file_name asc')}

  has_attached_file :image, styles: {
	  	thumbnail: '100x100>',
	  	small: '500x500>'
	  }
    	  
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  validates_attachment_file_name :image, :matches => [/png\Z/, /jpe?g\Z/]


  validates :image, :attachment_presence => true
  validates_with AttachmentPresenceValidator, :attributes => :image
  validates_with AttachmentSizeValidator, :attributes => :image, :less_than => 2.megabytes
  # validates :image_file_name, uniqueness: true
end
