class Size < ActiveRecord::Base
	validates :name, uniqueness: true, presence: true
	has_many :sizings
	has_many :posts, through: :sizings

	scope :default, -> {order('name asc')}

end
