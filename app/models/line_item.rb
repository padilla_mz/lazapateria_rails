class LineItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :post
  belongs_to :charge

  def total_price_line
  	# post.price * quantity
  	price * quantity
  end
end
