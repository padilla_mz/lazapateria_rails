class Static < ActiveRecord::Base
	validates :content, presence: true

	def to_param
		"#{id}-#{title.parameterize}"
	end
end
