class Post < ActiveRecord::Base
  belongs_to :user
  has_many :sizings
  has_many :sizes, through: :sizings
  has_many :taggings
  has_many :tags, through: :taggings
  has_many :likes, dependent: :destroy
  has_many :pictures, dependent: :destroy
  accepts_nested_attributes_for :pictures

  has_many :line_items
  has_many :charges, through: :line_items
  before_destroy :ensure_not_referenced_by_any_line_item

  has_many :variations, dependent: :destroy
  accepts_nested_attributes_for :variations, reject_if: :all_blank, allow_destroy: true


  # before_save :update_url

  
  
  validates :title, length: { maximum: 30, too_long: "%{count} letras son las maximas permitidas!" }, uniqueness: true, presence: true
  validates :sku, length: { in: 7..15, too_long: "%{count} caracteres son las maximas permitidas!", too_short: "%{count} caracteres son los minimos permitidos " }, uniqueness: true, presence: true 
  validates :price, numericality: {greater_than_or_equal_to: 2.00}
  validates :cost_price, numericality: {greater_than_or_equal_to: 1.00}
  # validates :fake_price, numericality: {greater_than: 1.00}
  validates :content, length: { maximum: 250, too_long: "%{count} letras son las maximas permitidas!" } 
  validates :content, :gender, :user_id, :category, presence: true

  # default_scope order('published_at desc')

  scope :default, -> {order('created_at desc')}

  scope :published, -> {default.includes(:tags).where(published: true)} 
  scope :featured, -> {published.where(featured: true).limit(16)}
  scope :recent, -> {published.limit(8)} 
  scope :popular, -> {published.order('likes_count desc').limit(9)}

  scope :men, -> {published.where(gender: :men).select(:user_id).distinct} 
  scope :women, -> {published.where(gender: :women).select(:user_id).distinct}

  scope :men_posts, -> {published.where(gender: :men)} 
  scope :women_posts, -> {published.where(gender: :women)}

  scope :zero_stock, -> {where("stock <= ?", 20).count}


  # def self.men_posts(artist="none")
  #    if artist == "none"
  #      published.where(gender: :men)
  #    else
  #      User.find_by_name!(artist).posts
  #    end
  # end

  # def self.women_posts(artist="none")
  #    if artist == "none"
  #      published.where(gender: :women)
  #    else
  #      User.find_by_name!(artist).posts
  #    end
  # end  


  Category = [:zapato, :default]
  Gender = [['Hombre', :men], ['Mujer', :women]]

  Sizes = [['S - chica', :small], ['M - mediana', :medium], ['L - grande', :large], ['XL - extra grande', :extra_large]]

  def self.more_from_this_artist(exclude, user_id)
    # offset = where
    where(user_id: user_id).where.not(id: exclude).limit(4)
  end

  def total_variation_stock
    self.variations.sum(:stock)
  end


  def self.tagged_with(name)
    Tag.find_by_name!(name).posts
  end


  def self.tag_counts
    #returns the most used tags

    Tag.select("tags.id, tags.name, count(taggings.tag_id) as count").joins(:taggings).group("taggings.tag_id, tags.id, tags.name").order("count desc").limit(8)
  end


  def tag_list
    #esto retorna un string
    tags.map(&:name).join(", ")

    #esto retorna un array
    # tags.map(&:name)
  end

  def tag_list=(names)
    self.tags = names.downcase.split(",").map do |n|
      Tag.find_or_create_by(name: n.strip)
    end
  end

  # def update_url
  #   self.url = title.parameterize
  # end

  def to_param
    "#{id}-#{title.parameterize}"
  end

  # def decrease_stock(q)
  #   upda
  # end


  private
    #ensure that there are no line items referencing this product
    def ensure_not_referenced_by_any_line_item
      if line_items.empty?
        return true
      else
        errors.add(:base, 'Line Items present')
        return false
      end
    end

end
