class Tag < ActiveRecord::Base
	validates :name, uniqueness: true, presence: true
	has_many :taggings
	has_many :posts, through: :taggings

	scope :default, -> {order('name asc')}


	# def self.destroy_tag
		
	# 	tags = self.all
	# 	tags.each do |tag|
	# 		# return tag
	# 		if tag.posts.count < 1
	# 			tag.destroy
	# 		end
	# 	end

	# end
end
