class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

      # can :read, Post
      # can :read, Tag


    if user.is? :admin
      can :manage, :all
    elsif user.is? :editor
      can :manage, [Post, Static, LineItem, Tag, Size]
      can [:create, :update], Charge
      can :read, :all
    elsif(user.is? :default or user.is? :artist)
      can :read, :all
      # can [:create, :read], Charge
      # can [:create, :update, :destroy], LineItem
      # can :read, [Like, Picture, Post, Promo, Static, Tag, User]
    else
      can :read, :all
    end


    
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
