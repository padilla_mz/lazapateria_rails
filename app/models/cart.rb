class Cart < ActiveRecord::Base
	has_many :line_items, dependent: :destroy

	def add_product(post_id, post_price, quantity, property='default', property_2='default')
		current_item = line_items.where(post_id: post_id).where(property: property).where(property_2: property_2).first
		quantity = quantity.to_i	
		if current_item
			current_item.quantity += quantity
		else
			current_item = LineItem.new(post_id: post_id, price: post_price, property: property, property_2: property_2, quantity: quantity)
			line_items << current_item
		end
		current_item
		
	end

	def update_product_quantity(product_id, quantity)
		item = line_items.where(product_id: product_id).first
		quantity = quantity.to_i

		if item
			item.quantity = quantity
		end
		item
	end

	def add_pp(product_id)
		current_item = line_items.find(product_id)
		if current_item
			current_item.quantity += 1			
		end
		current_item
	end

	def decrease(product_id, property='default', property_2='default')
		current_item = line_items.find(product_id)
		# current_item = line_items.where(post_id: post_id).where(property: property).where(property_2: property_2).first

		if current_item.quantity > 1
			current_item.quantity -= 1
		else
			current_item.destroy
		end
		current_item
	end

	def total_price_cart
		line_items.to_a.sum { |item| item.total_price_line }
	end
end
