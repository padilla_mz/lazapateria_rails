class Variation < ActiveRecord::Base
  belongs_to :post

  validates :size, presence: true
  # validates :stock, numericality: {greater_than: 0}

  scope :zero_stock, -> {where("stock <= ?", 10).count}

end
