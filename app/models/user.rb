class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :registerable

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :registerable


  has_many :charges
  has_many :posts
  has_many :likes


  has_attached_file :avatar, styles: {
	    small: '140x140>'
	  }


  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_attachment_file_name :avatar, :matches => [/png\Z/, /jpe?g\Z/]

  # validates :avatar, :attachment_presence => false
  # validates_with AttachmentPresenceValidator, :attributes => :avatar
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 0.5.megabytes



  validates :name, presence: true, uniqueness: true
  validates :email, uniqueness: true

  # validates :current_password, presence: true

  # validates :password, confirmation: true
  # validates :password_confirmation, presence: true

  # default_scope {order('role asc')}

  # scope :default, -> {order('name desc')}
  
  # users
  scope :artists, -> {where(role: :artist)}
  scope :artist_men, -> {artists.posts.where(gender: :men)}
  scope :artist_women, -> {artists.posts.where(gender: :women)}

  ### Roles, only one user can be :admin, that was done with the terminal in "rails c"

  # Roles = [:admin, :editor, :artist, :default]
  Roles = [:editor, :artist, :default]

  def likes?(post)
    post.likes.where(user_id: id).any?
  end

  def is?(requested_role)
    self.role == requested_role.to_s
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end


  after_create :send_notification

  def send_notification
    General.new_user(self).deliver  
    General.welcome(self).deliver  
  end



end
