class Promo < ActiveRecord::Base
	has_attached_file :promo_img, styles: {
  	large: '1000x250',
  	medium: '200x500'
  }

  validates_attachment_content_type :promo_img, :content_type => /\Aimage\/.*\Z/
  validates_attachment_file_name :promo_img, :matches => [/png\Z/, /jpe?g\Z/]

  validates :promo_img, :attachment_presence => true
  validates_with AttachmentPresenceValidator, :attributes => :promo_img
  validates_with AttachmentSizeValidator, :attributes => :promo_img, :less_than => 1.megabytes

  validates :title, :promo_img_file_name, uniqueness: true
  validates :title, presence: true

  Positions = [:slide, :index, :side, :show]

  # scope :default, -> {order('title desc')}
  default_scope {order('title asc')}


  scope :slide_ad, -> {where(position: :slide)}
  scope :index_ad, -> {where(position: :index)}
  scope :side_ad, -> {where(position: :side)}
  scope :show_ad, -> {where(position: :show)}
end
