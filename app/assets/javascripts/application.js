// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require turbolinks
//= require cocoon
//= require local_time
//= require_tree .

// $(function(){ $(document).foundation(); });


$(document).on('page:fetch',   function() { NProgress.start(); });

$(document).on('page:change',  function() { 

	// removiendo las variantes en los posts
	var $link_remove = $("#remove_link");
	var $link_add = $(".add_link");
	var $field = $("#field");
	var $hidden = $("#hidden");


	NProgress.done(); 
	$('.chosen-select').chosen({ 
		no_results_text: "nada encontrado",
		width: '100%'
	});

	$('#notice').delay(1300).fadeOut(1500);
	$('#alert').delay(1300).fadeOut(1500);
	

	$(document).foundation();

	// jivo chat init
	(function(){ var widget_id = '6UwKHjtlZ0';
	var s = document.createElement('script'); 
	s.type = 'text/javascript'; 
	s.async = true; 
	s.src = '//code.jivosite.com/script/widget/'+widget_id; 
	var ss = document.getElementsByTagName('script')[0]; 
	ss.parentNode.insertBefore(s, ss);})();
	//
});
$(document).on('page:restore', function() { NProgress.remove(); });





// twitter button
$(document).on('page:load', !function(d,s,id){

 	var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}
}(document, 'script', 'twitter-wjs'));

// $(function (){
// 	$link = $("#remove_link")
// 	$fields = $(".fields")
// 	$hidden = $("#hidden")

// 	$link.click(function(){
// 		$hidden.val("1");
// 		$fields.hide();

// 	});

// });










