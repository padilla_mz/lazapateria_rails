class ApplicationMailer < ActionMailer::Base
	default from: "Sistema la zapateria <hola@lazapateria.co>"

  layout 'mailer'
end
