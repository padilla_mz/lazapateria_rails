json.array!(@charges) do |charge|
  json.extract! charge, :id, :name, :full_address, :colonia, :city, :country, :state, :zip_code, :phone_number, :status, :ship_number, :delivery_service
  json.url charge_url(charge, format: :json)
end
