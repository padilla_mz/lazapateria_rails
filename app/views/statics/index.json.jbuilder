json.array!(@statics) do |static|
  json.extract! static, :id, :content, :published, :published_at
  json.url static_url(static, format: :json)
end
