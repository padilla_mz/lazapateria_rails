json.array!(@promos) do |promo|
  json.extract! promo, :id, :title, :description
  json.url promo_url(promo, format: :json)
end
