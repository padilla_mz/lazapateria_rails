class StoreController < ApplicationController
  skip_before_action :authenticate_user!

  
  def index
  	@body_class = "index"

    # random = rand(Promo.index_ad.count)
    # @promos_index = Promo.index_ad.offset(random).first
    # @promos_side = Promo.side_ad

    
    # @q = Post.search(params[:q])
    
    # @posts = @q.result(distinct: true).includes(:tags).order(published_at: :desc).paginate(per_page: @posts_per_page, page: params[:page])
    @slides = Promo.slide_ad
    @recent = Post.recent
    @popular = Post.popular
    @tags = Post.tag_counts

    # el metodo .includes sirve para anadir a la busqueda has_many tags o en su caso belongs_to
    
  end

  def women
    @body_class = "index"
    @posts = Post.women_posts.paginate(per_page: POSTS_PER_PAGE, page: params[:page])
  end

  def men
    @body_class = "index"
    @posts = Post.men_posts.paginate(per_page: POSTS_PER_PAGE, page: params[:page])
    
  end
end
