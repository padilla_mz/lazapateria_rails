class ChargesController < ApplicationController
  before_action :check_admin, only: [:index, :show]
  before_action :set_cart, only: [:new, :create, :cart, :oxxo, :oxxo_create]
  before_action :set_charge, only: [:show, :edit, :update, :destroy, :show_charge]




  # GET /charges
  # GET /charges.json
  def index
    @path = "charges"
    @body_class = "admin"

    @st = Charge.search(params[:q])
    @charges = @st.result(distinct: true).order(created_at: :desc).paginate(per_page: 50, page: params[:page])
  end

  # GET /charges/1
  # GET /charges/1.json
  def show
    @path = "charges"
    @body_class = "admin"
  end

  # GET /charges/new
  def new

    if @cart.line_items.empty?
      redirect_to root_path, notice: 'Tu carrito está vacio...'
    else
      @body_class = "pay"
      @charge = Charge.new

      @ship_price = calculate_shipping_amount(total_items, "medium")
      @total_price = (@cart.total_price_cart + @ship_price)
    end
  end

  # GET /charges/1/edit
  def edit
  end

  # POST /charges
  # POST /charges.json
  def create
    
    # redirect_to root_path if @cart.line_items.empty?

    ship_price = calculate_shipping_amount(total_items, "medium")

    total_pricec = (ship_price + @cart.total_price_cart) * 100
    total_pricecc = ship_price + @cart.total_price_cart

    order_sku = unique_id()

    begin
      charge = Conekta::Charge.create({
        amount: total_pricec.to_i,
        currency: "MXN",
        description: "Orden de la zapateria",
        reference_id: "#{order_sku}",
        card: params[:conektaTokenId],
        details: {
          name: params[:charge][:name],
          email: current_user.email,
          phone: params[:charge][:phone_number],
          billing_address: {
            company_name: "n/a",
            street1: params[:charge][:full_address],
            city: params[:charge][:city],
            state: params[:charge][:state],
            zip: params[:charge][:zip_code],
            phone: params[:charge][:phone_number],
            email: current_user.email
          }
        }
        #"tok_a4Ff0dD2xYZZq82d9"
      })
      # @charge = Charge.new(charge_params)
      @charge = Charge.new(
          user_id: current_user.id.to_i,
          name: params[:charge][:name],
          full_address: params[:charge][:full_address],
          colonia: params[:charge][:colonia],
          city: params[:charge][:city],
          country: params[:charge][:country],
          state: params[:charge][:state],
          zip_code: params[:charge][:zip_code],
          phone_number: params[:charge][:phone_number],
          livemode: charge.livemode,
          conekta_status: charge.status,
          amount: total_pricecc.to_i,
          conekta_id: order_sku,
          payment_type: "Tarjeta de credito",
          delivery_service: "DHL",
          currency: charge.currency
        )
      @charge.add_line_items_from_cart(@cart)

      if @charge.save
        @charge.line_items.each do |line_item|
          ## always variation exists
          variation = Variation.find_by(post_id: line_item.post.id, size: line_item.property).decrement(:stock, line_item.quantity)
          variation.save
        end


        

        @charge.stock_discounted = true
        @charge.save

        General.charge_paid(@charge).deliver
        General.charge_paid_admin(@charge).deliver
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        redirect_to show_charges_path(@charge)
      else
        redirect_to new_charge_path
      end

    rescue Conekta::ParameterValidationError => e
      #alguno de los parametros fueron invalidos
      puts e.message
    rescue Conekta::ProcessingError => e 
      # la tarjeta no pudo ser procesada, por ejemplo tarjeta declinada
      puts e.message
      redirect_to new_charge_path, alert: "Tarjeta no pudo ser procesada, la tarjeta fue declinada, intenta nuevamente o con otra tarjeta."
    rescue Conekta::Error => e
      #un error ocurrió que no sucede en el flujo normal de cobros como por ejemplo un auth_key incorrecto
      redirect_to new_charge_path, alert: "Tarjeta no pudo ser procesada, intenta nuevamente."  
      puts e.message
    
    end



  end

  # PATCH/PUT /charges/1
  # PATCH/PUT /charges/1.json
  def update


    respond_to do |format|
      if (@charge.stock_discounted == false) and (params[:charge][:status] == "enviado")
        @charge.update(charge_params)

        @charge.line_items.each do |line_item|
          ## always variation exists
          variation = Variation.find_by(post_id: line_item.post.id, size: line_item.property).decrement(:stock, line_item.quantity)
          variation.save
        end

        @charge.stock_discounted = true
        @charge.save
        format.html { redirect_to @charge, notice: 'Charge updated!'}
        format.json { render :show, status: :ok, location: @charge }
      else
        @charge.update(charge_params)
        format.html { redirect_to @charge, notice: 'Charge updated!'}
        format.json { render :show, status: :ok, location: @charge }
      end
    end

  end



  # DELETE /charges/1
  # DELETE /charges/1.json
  def destroy
    @charge.destroy
    respond_to do |format|
      format.html { redirect_to charges_url, notice: 'Charge was destroyed.' }
      format.json { head :no_content }
    end
  end


  # GET
  def cart
    if @cart.line_items.empty?
      redirect_to root_path, notice: 'Tu carrito esta vacio'
    else
      @body_class = "pay"
      @ship_price = calculate_shipping_amount(total_items, "medium")
    end
  end

  # GET
  def show_charge
    redirect_to root_path unless @charge.user.id == current_user.id
    @body_class = "show_charge"
  end

  # GET
  def oxxo
    redirect_to root_path if @cart.line_items.empty?

    @body_class = "pay"
    @charge = Charge.new

    @ship_price = calculate_shipping_amount(total_items, "medium")
    @total_price = (@cart.total_price_cart + @ship_price)

  end


  ## Payment with OXXO
  # POST
  def oxxo_create

    ship_price = calculate_shipping_amount(total_items, "medium")

    total_pricec = (ship_price + @cart.total_price_cart) * 100
    total_pricecc = ship_price + @cart.total_price_cart

    order_sku = unique_id()

    begin
      charge = Conekta::Charge.create({
        currency: "MXN",
        amount: total_pricec.to_i,
        description: "Orden de la zapateria",
        reference_id: "#{order_sku}",
        cash: {
          type: "oxxo"  
        },
        details: {
          name: params[:charge][:name],
          email: current_user.email,
          phone: params[:charge][:phone_number],
          billing_address: {
            company_name: "n/a",
            street1: params[:charge][:full_address],
            city: params[:charge][:city],
            state: params[:charge][:state],
            zip: params[:charge][:zip_code],
            phone: params[:charge][:phone_number],
            email: current_user.email
          }

        }
        #"tok_a4Ff0dD2xYZZq82d9"
      })
      # @charge = Charge.new(charge_params)
      @charge = Charge.new(
          user_id: current_user.id.to_i,
          name: params[:charge][:name],
          full_address: params[:charge][:full_address],
          colonia: params[:charge][:colonia],
          city: params[:charge][:city],
          country: params[:charge][:country],
          state: params[:charge][:state],
          zip_code: params[:charge][:zip_code],
          phone_number: params[:charge][:phone_number],
          livemode: charge.livemode,
          conekta_status: charge.status,
          amount: total_pricecc.to_i,
          conekta_id: order_sku,
          payment_type: "OXXO",
          payment_barcode: charge.payment_method.barcode,
          payment_bar_url: charge.payment_method.barcode_url,
          delivery_service: "DHL",
          currency: charge.currency
        )
      @charge.add_line_items_from_cart(@cart)
      
      if @charge.save
        General.oxxo_created(@charge).deliver
        General.oxxo_created_admin(@charge).deliver
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        # puts charge.payment_method.barcode_url
        redirect_to show_charges_path(@charge)
      else
        redirect_to oxxo_charges_path
      end
      
    rescue Conekta::ParameterValidationError => e
      #alguno de los parametros fueron invalidos
      puts e.message
    rescue Conekta::ProcessingError => e 
      # la tarjeta no pudo ser procesada
      puts e.message
    rescue Conekta::Error => e
      #un error ocurrió que no sucede en el flujo normal de cobros como por ejemplo un auth_key incorrecto
      redirect_to oxxo_charges_path, alert: "El cargo no pudo ser procesado"
      puts e.message
      
    end
    
  end

  def send_number
    @charge = Charge.find(params[:id])

    unless @charge.ship_number
      redirect_to charges_path, alert: "No hay numero de guia" 
    else
      General.shipped(@charge).deliver
      @charge.increment(:counter)
      @charge.save
      redirect_to @charge, notice: "Numero ha sido enviado!"
    end
  end




  private
    # Use callbacks to share common setup or constraints between actions.
    def set_charge
      @charge = Charge.find(params[:id])
    end

    def total_items
      item_count = 0
      @cart.line_items.each do |item|
        item_count += item.quantity
      end
      item_count
    end

    def calculate_shipping_amount(items, size="medium")
      if size == "big"
        interval = 1.0
      elsif size == "medium"
        interval = 2.0
      elsif size == "small"
        interval = 3.0
      end

      return ((items/interval).ceil) * SHIP_PRICE 
    end

    def unique_id
      time = Time.new
      "#{time.min}#{time.hour}#{time.day}#{time.month}#{time.year}#{current_user.name.parameterize}"
      # "#{time.sec}#{time.min}#{time.hour}#{time.day}#{time.month}#{time.year}#{current_user.name.parameterize}"
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def charge_params
      params.require(:charge).permit(:user_id, :name, :full_address, :colonia, :city, :country, :state, :zip_code, :phone_number, :status, :ship_number, :delivery_service)
    end
    def check_admin
      unless current_user.is?(:admin) or current_user.is?(:editor)
        redirect_to root_path, alert: "?"
      end
    end
end
