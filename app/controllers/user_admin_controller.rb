class UserAdminController < ApplicationController
	before_action :set_user
  skip_before_action :authenticate_user!, only: [:index]

  def index
  	@a = "color"
    @products = @user.posts
  	@likes = Like.where(user_id: @user.id)
  end

  def ventas
    
  end


  private
    # Use callbacks to share common setup or constraints between actions.
  	def set_user
  		@user = User.find(params[:id])
  	end
end
