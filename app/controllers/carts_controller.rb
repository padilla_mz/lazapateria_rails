class CartsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :set_cart, only: [:show, :edit, :update, :destroy]
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_cart

  # GET /carts
  # GET /carts.json
  def index
    # @carts = Cart.all
    redirect_to root_path
  end

  # GET /carts/1
  # GET /carts/1.json
  def show
    redirect_to root_path

  end

  # GET /carts/new
  def new
    # @cart = Cart.new
    redirect_to root_path

  end

  # GET /carts/1/edit
  def edit
    redirect_to root_path

  end

  # POST /carts
  # POST /carts.json
  def create
    @cart = Cart.new(cart_params)

    respond_to do |format|
      if @cart.save
        format.html { redirect_to @cart, notice: 'Cart was successfully created.' }
        format.json { render :show, status: :created, location: @cart }
      else
        format.html { render :new }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /carts/1
  # PATCH/PUT /carts/1.json
  def update
    respond_to do |format|
      if @cart.update(cart_params)
        format.html { redirect_to @cart, notice: 'Cart was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart }
      else
        format.html { render :edit }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    if @cart.id == session[:cart_id]
      @cart.destroy
      session[:cart_id] = nil
      respond_to do |format|
        format.html { redirect_to root_path, notice: 'Carrito ha sico eliminado' }
        format.json { head :no_content }
      end
      
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = Cart.find(params[:id])
    end

    def cart_params
      params[:cart]
    end

    def check_admin
      unless current_user.is?(:admin) or current_user.is?(:editor)
        redirect_to root_path, alert: "?"
      end
    end

    def invalid_cart

      # OrderNotifier.error_ocured("email: intento de ver carrito invalido #{params[:id]} siendo #{session[:cart_id]}").deliver

      logger.error "Attempt to access invalid cart #{params[:id]}"
      redirect_to cart_path(session[:cart_id])
    end
end
