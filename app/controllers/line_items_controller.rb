class LineItemsController < ApplicationController

  # skip_before_action :authenticate_user!, only: [:create, :decrease, :destroy]
  before_action :set_cart, only: [:create, :decrease, :destroy, :increase]
  before_action :set_line_item, only: [:show, :edit, :update, :destroy]
  # before_action :set_product


  # GET /line_items
  # GET /line_items.json
  def index
    redirect_to root_path unless current_user.is?(:admin) or current_user.is?(:editor)

    @line_items = LineItem.all
  end

  # GET /line_items/1
  # GET /line_items/1.json
  def show
    redirect_to root_path unless current_user.is?(:admin) or current_user.is?(:editor)

  end

  # GET /line_items/new
  def new
    redirect_to root_path unless current_user.is?(:admin) or current_user.is?(:editor)

    @line_item = LineItem.new
  end

  # GET /line_items/1/edit
  def edit
    redirect_to root_path unless current_user.is?(:admin) or current_user.is?(:editor)

  end

  # POST /line_items
  # POST /line_items.json
  def create

    product = Post.find(params[:product_id])
    quantity = params[:line_item][:quantity]

    @line_item = @cart.add_product(product.id, product.price, quantity, params[:line_item][:property])

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to product_path(product), notice: 'Producto añadido al carrito' }
        format.js { @current_item = @line_item }
      else
        format.html { render action: 'new' }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end


  end



  # PATCH/PUT /line_items/1
  # PATCH/PUT /line_items/1.json
  def update
    respond_to do |format|
      if @line_item.update(line_item_params)
        format.html { redirect_to root_path, notice: 'Line item was successfully updated.' }
        format.json { render :show, status: :ok, location: root_path }
      else
        format.html { render :edit }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_multiple
    @product_ids = params[:product_ids]
    @quantities = params[:product_quantities]

    @product_ids.zip(@quantities).each do |product_id, quantity|
      @line_item = @cart.update_product_quantity(product_id, quantity)
      @line_item.save
    end


    respond_to do |f|
      if @line_item.save
        f.html {redirect_to cart_charges_path, notice: 'productos actualizados!!'}
      else
        f.html {redirect_to cart_charges_path, notice: 'no paso nada loco :('}
      end
    end
  end

  # DELETE /line_items/1
  # DELETE /line_items/1.json
  def destroy
    @line_item.destroy
    respond_to do |format|
      format.html { redirect_to cart_charges_path, notice: 'Producto removido' }
      format.json { head :no_content }
      format.js { @current_item = @line_item }

    end
  end


  def increase
    item = LineItem.find(params[:id])

    @line_item = @cart.add_pp(item)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to cart_charges_path, notice: 'itemmmmm updated!'}
        format.js { @current_item = @line_item }
      else
        format.html { render action: "new" }
      end
    end
  end

  def decrease
    product = LineItem.find(params[:id])
  
    @line_item = @cart.decrease(product)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to cart_charges_path, notice: 'itemmmmm updated!'}
        format.js { @current_item = @line_item }
      else
        format.html { render action: "edit" }
      end
    end
  end




  private
    # Use callbacks to share common setup or constraints between actions.
    def set_line_item
      @line_item = LineItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def line_item_params
      params.require(:line_item).permit(:post_id, :property, :property_2)
    end
end
