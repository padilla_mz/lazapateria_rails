class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  
  skip_before_action :authenticate_user!, only: [:index, :show]


  # GET /posts
  # GET /posts.json
  def index

    @body_class = "index"
    if params[:tag]
      @posts = Post.tagged_with(params[:tag]).where(published: true).paginate(per_page: POSTS_PER_PAGE, page: params[:page])
    else
      # @posts = @q.result(distinct: true).includes(:tags).order(published_at: :desc).paginate(per_page: @posts_per_page, page: params[:page])
      @posts = @q.result(distinct: true).includes(:tags).published.paginate(per_page: POSTS_PER_PAGE, page: params[:page])
      # el metodo .includes sirve para anadir a la busqueda has_many tags o en su caso belongs_to
    end

  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @body_class = "show"
    @pictures = @post.pictures
    if @post.user
      @more = Post.more_from_this_artist(@post.id, @post.user.id)
    end
    @line_item = LineItem.new
  end

  # GET /posts/new
  def new
    @path = "index"
    @body_class = "admin"


    @post = Post.new
    @post.variations.build
    image_count = 4
    @image_count = image_count.to_int

  end

  # GET /posts/1/edit
  def edit
    # @post.variations.build
    @body_class = "admin"

    @path = "index"
    @image_count = 4 - @post.pictures.count

  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    # @post.stock = @post.total_variation_stock

    # con Devise y modificando los models para post con belongs_to :user
    # y user con has_many :posts
    # devise proporciona un helper o metodo current_user

    # @post = current_user.posts.new(post_params)


    respond_to do |format|
      if @post.save
        @post.update(stock: @post.total_variation_stock)

        post = params[:post]
        if post[:image_0] or post[:image_1] or post[:image_2] or post[:image_3] or post[:image_4]
          post_0 = Picture.create(image: post[:image_0], post_id: @post.id)
          post_1 = Picture.create(image: post[:image_1], post_id: @post.id)
          post_2 = Picture.create(image: post[:image_2], post_id: @post.id)
          post_3 = Picture.create(image: post[:image_3], post_id: @post.id)
          post_4 = Picture.create(image: post[:image_4], post_id: @post.id)
        end

        format.html { redirect_to admin_path, notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    @image_count = 4 - @post.pictures.count

    respond_to do |format|
      if @post.update(post_params)
        @post.update(stock: @post.total_variation_stock)

        post = params[:post]
        if post[:image_0] or post[:image_1] or post[:image_2] or post[:image_3] or post[:image_4]
          post_0 = Picture.create(image: post[:image_0], post_id: @post.id)
          post_1 = Picture.create(image: post[:image_1], post_id: @post.id)
          post_2 = Picture.create(image: post[:image_2], post_id: @post.id)
          post_3 = Picture.create(image: post[:image_3], post_id: @post.id)
          post_4 = Picture.create(image: post[:image_4], post_id: @post.id)
        end
       
        format.html { redirect_to admin_path, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to admin_path, notice: 'Post destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:category, :title, :content, :published_at, :published, :user_id, :pictures, :sku, :weight, :price, :cost_price, :fake_price, :featured, :gender, tag_ids: [], size_ids: [], variations_attributes: [:size, :stock, :_destroy, :id])

      # params.require(:post).permit(:title, :content, :published_at, :published, :user_id, :sku, :weight, :price, :cost_price, :featured, :category, :likes_count, :stock)
    end
end
