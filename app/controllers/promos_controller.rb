class PromosController < ApplicationController
  before_action :check_admin
  before_action :set_promo, only: [:show, :edit, :update, :destroy]
  # skip_before_action :authenticate_user!, only: [:show]


  # GET /promos
  # GET /promos.json
  def index
    @body_class = "admin"
    @path = "promos"
    @promos = Promo.all
  end

  # GET /promos/1
  # GET /promos/1.json
  def show
  end

  # GET /promos/new
  def new
    @path = "promos"

    @promo = Promo.new
  end

  # GET /promos/1/edit
  def edit
    @path = "promos"

  end

  # POST /promos
  # POST /promos.json
  def create
    @promo = Promo.new(promo_params)

    respond_to do |format|
      if @promo.save
        format.html { redirect_to admin_promos_path, notice: 'Promo created.' }
        format.json { render :show, status: :created, location: admin_promos_path }
      else
        format.html { render :new }
        format.json { render json: @promo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /promos/1
  # PATCH/PUT /promos/1.json
  def update
    respond_to do |format|
      if @promo.update(promo_params)
        format.html { redirect_to admin_promos_path, notice: 'Promo was successfully updated.' }
        format.json { render :show, status: :ok, location: admin_promos_path }
      else
        format.html { render :edit }
        format.json { render json: @promo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /promos/1
  # DELETE /promos/1.json
  def destroy
    @promo.destroy
    respond_to do |format|
      format.html { redirect_to admin_promos_path, notice: 'Promo destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_promo
      @promo = Promo.find(params[:id])
    end

    def check_admin
      unless current_user.is?(:admin) or current_user.is?(:editor)
        redirect_to root_path, alert: "?"
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def promo_params
      params.require(:promo).permit(:title, :description, :promo_img, :url, :position)

    end
end
