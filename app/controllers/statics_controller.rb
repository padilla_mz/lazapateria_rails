class StaticsController < ApplicationController
  before_action :set_static, only: [:show, :edit, :update, :destroy]
  skip_before_action :authenticate_user!, only: [:show]
  before_action :check_admin, except: [:show]


  # GET /statics
  # GET /statics.json
  def index

    @body_class = "admin"

    @path = "pages"
    @st = Static.search(params[:q])
    @statics = @st.result(distinct: true).order(published_at: :desc).paginate(per_page: 20, page: params[:page])
  end

  # GET /statics/1
  # GET /statics/1.json
  def show
    redirect_to root_path, notice: "La página no existe" if @static.published == false
    @body_class = "show"
  end

  # GET /statics/new
  def new
    @path = "pages"
    @static = Static.new
  end

  # GET /statics/1/edit
  def edit
    @path = "pages"
  end

  # POST /statics
  # POST /statics.json
  def create
    @static = Static.new(static_params)

    respond_to do |format|
      if @static.save
        format.html { redirect_to statics_path, notice: 'Page created.' }
        format.json { render :show, status: :created, location: statics_path }
      else
        format.html { render :new }
        format.json { render json: @static.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /statics/1
  # PATCH/PUT /statics/1.json
  def update
    respond_to do |format|
      if @static.update(static_params)
        format.html { redirect_to statics_path, notice: 'Page updated.' }
        format.json { render :show, status: :ok, location: statics_path }
      else
        format.html { render :edit }
        format.json { render json: @static.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /statics/1
  # DELETE /statics/1.json
  def destroy
    @static.destroy
    respond_to do |format|
      format.html { redirect_to statics_path, notice: 'Page destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_static
      @static = Static.find(params[:id])
    end

    def check_admin
      unless current_user.is?(:admin) or current_user.is?(:editor)
        redirect_to root_path, alert: "?"
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def static_params
      params.require(:static).permit(:content, :published, :published_at, :title)
    end
end
