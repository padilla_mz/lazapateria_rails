class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  
  before_action :authenticate_user!

  # include CurrentCart
  before_action :set_stuff, :set_cart

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: 'cancan error'
  end

  def set_stuff
    @pages = Static.where(published: true)

    @q = Post.search(params[:q])
    
  end

  private
    def set_cart
      @cart = Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      @cart = Cart.create
      session[:cart_id] = @cart.id 
    end

  protected
  	def configure_permitted_parameters
  		devise_parameter_sanitizer.for(:sign_up) << :name
      devise_parameter_sanitizer.for(:sign_up) << :twitter
      # devise_parameter_sanitizer.for(:sign_up) << :avatar
  		# devise_parameter_sanitizer.for(:sign_up) << :role
  		devise_parameter_sanitizer.for(:account_update) << :name
      devise_parameter_sanitizer.for(:account_update) << :twitter
      devise_parameter_sanitizer.for(:account_update) << :avatar
      devise_parameter_sanitizer.for(:account_update) << :web_profile
  		# devise_parameter_sanitizer.for(:account_update) << :role
  	end

end
