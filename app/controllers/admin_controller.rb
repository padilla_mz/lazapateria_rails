class AdminController < ApplicationController
	
  before_action :check_admin
  before_action :set_user, only: [:edit, :update, :destroy_user]


  def index
    @body_class = "admin"
    @path = "index"
    # @q = Post.search(params[:q])
  	@posts = @q.result(distinct: true).order(created_at: :desc).paginate(per_page: 200, page: params[:page]) 
  	# @users = User.all 

    @stock = Variation.sum(:stock)

    @zero_stock = Variation.zero_stock
    # @charges = Charge.charges_in_months(1).count
  	
  end

  def users
    @body_class = "admin"

    if current_user.is?(:admin) or current_user.is?(:editor)
      @path = "users"
      @st = User.search(params[:q])
      @users = @st.result(distinct: true).order(role: :asc).paginate(per_page: 100, page: params[:page])
    else
      redirect_to root_path, notice: "?"
    end
  end

  ## user role edit
  def edit
    redirect_to admin_path unless current_user.is?(:admin)
    @body_class = "admin"
  end

  ## user role update
  def update
    @user.update(user_params)
    redirect_to admin_users_path, notice: "usuario actualizado!"
  end
  ## new user get
  def new
    redirect_to admin_path unless current_user.is?(:admin)
    @body_class = "admin"
    @user = User.new
  end
  ## new user post
  def create
    redirect_to admin_path unless current_user.is?(:admin)

    @user = User.new(user_params2)
    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_users_path, notice: 'User a sido creado.' }
      else
        format.html { redirect_to user_new_path, notice: 'Intenta nuevamente.' }
      end
    end
    
  end

  # DELETE
  def destroy_user

    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_path, notice: 'Usuario destruido.' }
    end
  end

  private

    def check_admin
      unless current_user.is?(:admin) or current_user.is?(:editor)
        redirect_to root_path, alert: "?"
      end
    end

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:role)
      
    end
    def user_params2
      params.require(:user).permit(:role, :name, :email, :password, :password_confirmation)
    end
end
