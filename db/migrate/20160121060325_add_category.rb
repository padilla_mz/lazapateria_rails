class AddCategory < ActiveRecord::Migration
  def change
  	change_column :posts, :category, :string, default: 'default'
  end
end
