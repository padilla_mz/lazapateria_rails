class CreateSizings < ActiveRecord::Migration
  def change
    create_table :sizings do |t|
      t.references :size, index: true, foreign_key: true
      t.references :post, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
