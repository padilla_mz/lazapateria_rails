class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.string :name
      t.string :full_address
      t.string :colonia
      t.string :city
      t.string :country
      t.string :state
      t.string :zip_code
      t.string :phone_number
      t.string :status, default: 'en proceso'
      t.string :ship_number
      t.string :delivery_service, default: 'otro'
      t.string :conekta_id
      t.string :livemode
      t.string :conekta_status
      t.integer :amount
      t.string :currency
      t.string :payment_type
      t.string :payment_barcode
      t.string :payment_bar_url
      t.references :user, index: true, foreign_key: true
      t.integer :counter, default: 0
      t.boolean :stock_discounted, default: false

      t.timestamps null: false
    end
  end
end
