class CreatePromos < ActiveRecord::Migration
  def change
    create_table :promos do |t|
      t.string :title
      t.text :description
      t.string :url, default: ''
      t.string :position

      t.timestamps null: false
    end
  end
end
