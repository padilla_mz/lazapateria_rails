class ChangeTypeOfValue < ActiveRecord::Migration
  def change
  	change_column :variations, :stock, :integer, default: 0
  end
end
