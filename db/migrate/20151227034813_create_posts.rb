class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.datetime :published_at
      t.boolean :published, default: true
      t.references :user, index: true, foreign_key: true
      t.string :sku
      t.decimal :weight, precision: 8, scale: 3, default: 0.0
      t.decimal :price, precision: 8, scale: 3, default: 0.0
      t.decimal :cost_price, precision: 8, scale: 3, default: 0.0
      t.boolean :featured, default: false
      t.string :category
      t.integer :likes_count, default: 0
      t.integer :stock, default: 0

      t.timestamps null: false
    end
  end
end
