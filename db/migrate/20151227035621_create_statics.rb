class CreateStatics < ActiveRecord::Migration
  def change
    create_table :statics do |t|
      t.text :content
      t.boolean :published, default: false
      t.datetime :published_at
      t.string :title

      t.timestamps null: false
    end
  end
end
