class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.references :cart, index: true, foreign_key: true
      t.integer :quantity, default: 1
      t.decimal :price, precision: 8, scale: 2
      t.references :post, index: true, foreign_key: true
      t.references :charge, index: true, foreign_key: true
      t.string :property
      t.string :property_2

      t.timestamps null: false
    end
  end
end
