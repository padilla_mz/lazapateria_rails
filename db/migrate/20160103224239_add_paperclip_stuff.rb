class AddPaperclipStuff < ActiveRecord::Migration
  def change
  	add_attachment :users, :avatar
  	add_attachment :pictures, :image
  	add_attachment :promos, :promo_img
  end
  def down
    remove_attachment :users, :avatar
    remove_attachment :pictures, :image
    remove_attachment :promos, :promo_img
  end
end
