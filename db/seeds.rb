# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


users = User.create([
		{name: 'Angel Padilla', email: 'angelpadillam@gmail.com', role: 'admin', password: '19891001aaa', password_confirmation: '19891001aaa'},
		{name: 'Don Pepe', email: 'pepe@gmail.com', role: 'default', password: '19891001aaa', password_confirmation: '19891001aaa'},
		{name: 'Jonathan Lopez', email: 'begui@hotmail.com', role: 'editor', password: '12345678', password_confirmation: '12345678'},
	])

200.times do |x|	 
	Post.create(
			title: "zapato 0#{x}",
			content: "esto es solo una prueba y por lo tanto no lo leas he dicho!!",
			published: true,
			sku: "asodij97#{x}",
			weight: 99.00,
			price: 100.00,
			cost_price: 80.00
		)
end

tags = Tag.create([
		{name:'retro'},
		{name:'vintage'},
		{name:'hipster'},
		{name:'mujer'},
		{name:'hombre'},
		{name:'niño'},
		{name:'negro'},
		{name:'verde'},
		{name:'rojo'},
	])

sizes = Size.create([
		{name: '24'},
		{name: '24 ½'},
		{name: '25'},
		{name: '25 ½'},
		{name: '26'},
		{name: '26 ½'},
		{name: '27'},
		{name: '27 ½'},
		{name: '28'},
		{name: '28 ½'},
		{name: '29'},
		{name: '29 ½'},
		{name: '30'},
		{name: '31'},
	])