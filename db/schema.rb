# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160422023306) do

  create_table "carts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "charges", force: :cascade do |t|
    t.string   "name"
    t.string   "full_address"
    t.string   "colonia"
    t.string   "city"
    t.string   "country"
    t.string   "state"
    t.string   "zip_code"
    t.string   "phone_number"
    t.string   "status",           default: "en proceso"
    t.string   "ship_number"
    t.string   "conekta_id"
    t.string   "livemode"
    t.string   "conekta_status"
    t.integer  "amount"
    t.string   "currency"
    t.string   "payment_type"
    t.string   "payment_barcode"
    t.string   "payment_bar_url"
    t.integer  "user_id"
    t.integer  "counter",          default: 0
    t.boolean  "stock_discounted", default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "delivery_service", default: "DHL"
  end

  add_index "charges", ["user_id"], name: "index_charges_on_user_id"

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "likes", ["post_id"], name: "index_likes_on_post_id"
  add_index "likes", ["user_id"], name: "index_likes_on_user_id"

  create_table "line_items", force: :cascade do |t|
    t.integer  "cart_id"
    t.integer  "quantity",                           default: 1
    t.decimal  "price",      precision: 8, scale: 2
    t.integer  "post_id"
    t.integer  "charge_id"
    t.string   "property"
    t.string   "property_2"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "line_items", ["cart_id"], name: "index_line_items_on_cart_id"
  add_index "line_items", ["charge_id"], name: "index_line_items_on_charge_id"
  add_index "line_items", ["post_id"], name: "index_line_items_on_post_id"

  create_table "pictures", force: :cascade do |t|
    t.integer  "post_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "pictures", ["post_id"], name: "index_pictures_on_post_id"

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "published_at"
    t.boolean  "published",                            default: true
    t.integer  "user_id"
    t.string   "sku"
    t.decimal  "weight",       precision: 8, scale: 3, default: 0.0
    t.decimal  "price",        precision: 8, scale: 3, default: 0.0
    t.decimal  "cost_price",   precision: 8, scale: 3, default: 0.0
    t.boolean  "featured",                             default: false
    t.string   "category",                             default: "zapato"
    t.integer  "likes_count",                          default: 0
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.string   "gender"
    t.decimal  "fake_price",   precision: 8, scale: 3, default: 0.0
    t.integer  "stock",                                default: 0
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id"

  create_table "promos", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "url",                    default: ""
    t.string   "position"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "promo_img_file_name"
    t.string   "promo_img_content_type"
    t.integer  "promo_img_file_size"
    t.datetime "promo_img_updated_at"
  end

  create_table "sizes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sizings", force: :cascade do |t|
    t.integer  "size_id"
    t.integer  "post_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "variation_id"
  end

  add_index "sizings", ["post_id"], name: "index_sizings_on_post_id"
  add_index "sizings", ["size_id"], name: "index_sizings_on_size_id"

  create_table "statics", force: :cascade do |t|
    t.text     "content"
    t.boolean  "published",    default: false
    t.datetime "published_at"
    t.string   "title"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "taggings", ["post_id"], name: "index_taggings_on_post_id"
  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id"

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",        null: false
    t.string   "encrypted_password",     default: "",        null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,         null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,         null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "name"
    t.string   "twitter"
    t.string   "web_profile"
    t.string   "role",                   default: "default"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true

  create_table "variations", force: :cascade do |t|
    t.integer  "stock",      default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "post_id"
    t.string   "size"
  end

  add_index "variations", ["post_id"], name: "index_variations_on_post_id"

end
