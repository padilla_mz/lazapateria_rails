Rails.application.routes.draw do
  resources :sizes, :tags, :pictures, :promos, :statics, :carts
  devise_for :users



  resources :charges do
    #ejemplo de ruta añadida
    collection do 
      get 'cart', to: 'charges#cart', as: :cart
      get 'oxxo', to: 'charges#oxxo', as: :oxxo 
      get 'show/:id', to: 'charges#show_charge', as: :show 
      post 'oxxo/pay', to: 'charges#oxxo_create', as: :oxxo_create
    end

    member do
      put 'send_number'
    end
  end

  resources :line_items do
    put 'decrease', on: :member
    put 'increase', on: :member
  end


  get "linea/:id", to: 'user_admin#index', as: :user_profile
  get "linea/:id/ventas", to: 'user_admin#ventas', as: :user_ventas


  get "admin" => 'admin#index'
  get "admin/charges" => 'charges#index'
  get "admin/statics" => 'statics#index'
  get "admin/promos" => 'promos#index'
  get "admin/users" => 'admin#users'
  get "admin/tags" => 'tags#index'
  get "admin/sizes" => 'sizes#index'

  get "admin/users/:id/edit" => 'admin#edit', as: :user_edit
  delete "admin/users/:id" => 'admin#destroy_user', as: :user_delete
  patch "admin/users/:id" => 'admin#update'
  put "admin/users/:id" => 'admin#update', as: :user_edit_s

  get "admin/users/new" => 'admin#new', as: :user_new
  post "admin/users" => 'admin#create', as: :user_create



  # modifing the route for display ther products
  get "products", to: 'posts#index', as: :products
  get "products/:id", to: 'posts#show', as: :product

  get "women", to: 'store#women', as: :women
  get "men", to: 'store#men', as: :men

  # resources :products, controller: 'posts', as: :post

  resources :posts do
    resource :like, module: :posts
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".


  # to get the posts filtered by tag
  get 'tag/:tag', to: 'posts#index', as: :tag_find

  # You can have the root of your site routed with "root"
  root 'store#index'

  
  









































  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
