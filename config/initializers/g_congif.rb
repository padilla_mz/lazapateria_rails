Min_sales_month = 60

SITE_NAME = "lazapateria.co"
SITE_SUBTITLE = ""
SITE_URL = "http://lazapateria.co/"
META_DESCRIPTION = "Arte mexicano y urbano"
META_KEYWORDS = ""
EMAIL = "hola@lazapateria.co"

#will_paginate
POSTS_PER_PAGE = 12

#Locale
TWITTER_LOCALE = "en" #default "en"
FACEBOOK_LOCALE = "en_US" #default "en_US"
GOOGLE_LOCALE =  "es-419" #default "en", espanol latinoamerica "es-419"

#ANALYTICS
GOOGLE_ANALYTICS_ID = "UA-48771067-3" #your tracking id

#SOCIAL

DISQUS_SHORTNAME = "lazapateria"
TWITTER_USERNAME = "lazapateria1"
FACEBOOK_URL = "https://www.facebook.com/lazapateria.co"
GOOGLE_PLUS_URL = ""

#FedEx
SHIP_NAME = "DHL"
SHIP_PRICE = 110
